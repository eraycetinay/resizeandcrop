var gm = require('gm').subClass({ imageMagick: true });
var fs = require('fs');
var dir='./uploads/';

if (!fs.existsSync(dir+'backup_city')){fs.mkdirSync(dir+'backup_city');}
if (!fs.existsSync(dir+'city')){fs.mkdirSync(dir+'city');}
if (!fs.existsSync(dir+'city/300x300/')){fs.mkdirSync(dir+'city/300x300/');}
if (!fs.existsSync(dir+'city/600x300/')){fs.mkdirSync(dir+'city/600x300/');}

process.argv.forEach(function (val, index, array) {

if(val=="copy"){
    fs.readdir(dir, (err, files) => {
    	files.forEach(file => { 
        	if ( /\.(jpe?g|png|gif|bmp)$/i.test(file) ) {
                gm(dir+file).size(function(err, size) {
                    if (size.width == 1920 && size.height == 500) {
                    	fs.createReadStream(dir+file).pipe(fs.createWriteStream(dir+'backup_city/'+file));
                        gm(dir+file)
                            .resize(300, 300 + '^')
                            .gravity('center')
                            .crop(300, 300)
                            .quality(82)
                            .write(dir+'city/300x300/'+file, function(err) {});
                        gm(dir+file)
                            .resize(600, 300 + '^')
                            .gravity('center')
                            .crop(600, 300)
                            .quality(82)
                            .write(dir+'city/600x300/'+file, function(err) {});
                        gm(dir+file)
                            .quality(82)
                            .write(dir+'city/'+file, function(err) {});
                        console.log(file);
                    }
                })
            }
        });
    })
 }
if(val=="clean"){
    fs.readdir(dir, (err, files) => {
    	files.forEach(file => { 
        	if ( /\.(jpe?g|png|gif|bmp)$/i.test(file) ) {
                gm(dir+file).size(function(err, size) {
                    if (size.width == 1920 && size.height == 500) {
                    	fs.unlink(dir+file);
                    	console.log(file);
                    }
                })
            }
        });
    })
 }

});